Relax at one of Bristol's most modern and luxurious unisex hair salons when you visit BME. As an ever-growing team of stylists who are passionate about creating the perfect hairstyle for clients, we utilise 21 years of combined experience to ensure you leave our salon feeling and looking amazing.

Address: 5.7 BME, Bristol,  BS4 3EH, United Kingdom

Phone: +44 117 444 9330

Website: [https://www.bmesalon.co.uk](https://www.bmesalon.co.uk)
